"""Test msofficepy.access"""
import pytest

import pandas as pd
import pyodbc

import msofficepy


# Set ENABLE_TEST to True to test.
# Aware testcases here needs 'Microsoft Access Driver (*.mdb, *.accdb)'.
# Check by `pyodbc.drivers()` command. For example:
# ```
# >>> import pyodbc
# >>> pyodbc.drivers()
# ['SQL Server', 'Microsoft Access Driver (*.mdb, *.accdb)',
# 'Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)',
# 'Microsoft Access dBASE Driver (*.dbf, *.ndx, *.mdx)',
# 'Microsoft Access Text Driver (*.txt, *.csv)']
# ````
ENABLE_TEST = False
SKIP_IF = pytest.mark.skipif(
    not ENABLE_TEST,
    reason='unsure if access driver is installed',
)


@SKIP_IF
class TestFileToDataframe:
    """Test accdb.file_to_dataframe"""

    def test_raise_given_invalid_path(self, tmp_path):
        """test_raise_given_invalid_path"""
        accdb_path = tmp_path.joinpath("invalid.accdb").resolve()

        sql = """SELECT * FROM MyTable;"""

        with pytest.raises(pyodbc.Error) as e:
            assert msofficepy.access.file_to_dataframe(
                path=str(accdb_path), sql=sql
            )
        assert e._excinfo[1].args[0] == "HY000"

    def test_raise_given_invalid_sql(
            self,
            readonly_common_accdb
    ):
        """test_raise_pyodbc_error_given_syntax_error_sql"""
        accdb_path = readonly_common_accdb["one_table_accdb.accdb"]

        sql = """SELECT invalidColumn FROM MyTable;"""

        with pytest.raises(pd.io.sql.DatabaseError):
            assert msofficepy.access.file_to_dataframe(
                path=str(accdb_path), sql=sql
            )

    def test_return_empty_df_when_sql_returns_no_record(
            self,
            readonly_common_accdb
    ):
        """return_empty_df_when_sql_returns_no_record"""
        accdb_path = readonly_common_accdb["one_empty_table_accdb.accdb"]

        sql = """SELECT * FROM MyTable;"""

        w_df = msofficepy.access.file_to_dataframe(
            path=str(accdb_path), sql=sql
        )

        assert len(w_df) == 0

    def test_return_non_empty_df_when_sql_returns_records(
            self,
            readonly_common_accdb
    ):
        """return_non_empty_df_when_sql_returns_records"""
        accdb_path = readonly_common_accdb["one_table_accdb.accdb"]

        sql = """
            SELECT
            t1.LastName,
            t1.FirstName,
            t1.DateOfBirth,
            t2.LastName as might_be_family_LastName,
            t2.FirstName as might_be_family_FirstName,
            t2.DateOfBirth as might_be_family_DateOfBirth
            FROM MyTable as t1
            LEFT JOIN MyTable as t2
            ON t1.LastName = t2.LastName
            AND (
                t1.FirstName <> t2.FirstName
                OR t1.DateOfBirth <> t2.DateOfBirth
            )
        ;"""

        w_df = msofficepy.access.file_to_dataframe(
            path=str(accdb_path), sql=sql
        )
        print(w_df.info())

        assert len(w_df) == 7


@SKIP_IF
class TestZipToDataframe:
    """Test accdb.zip_to_dataframe"""

    def test_raise_given_invalid_path(self, tmp_path):
        """test_raise_given_invalid_path"""
        zip_path = tmp_path.joinpath("invalid.zip").resolve()
        zip_member = zip_path.stem + ".accdb"

        sql = """SELECT * FROM MyTable;"""

        with pytest.raises(FileNotFoundError):
            assert msofficepy.access.zip_to_dataframe(
                path=str(zip_path), zip_member=zip_member, sql=sql
            )

    def test_raise_given_invalid_zip_member(
            self,
            readonly_common_accdb
    ):
        """return_non_empty_df_when_sql_returns_records"""
        zip_path = readonly_common_accdb["one_table_accdb.zip"]
        zip_member = "invalid.accdb"

        sql = """SELECT * FROM MyTable;"""

        with pytest.raises(KeyError):
            assert msofficepy.access.zip_to_dataframe(
                path=str(zip_path), zip_member=zip_member, sql=sql
            )

    @pytest.mark.parametrize("zip_name,expected", [
        ("one_table_accdb.zip", 7),
        ("one_table_日本語ファイル名_accdb.zip", 7),
    ])
    def test_return_non_empty_df_when_sql_returns_records(
            self,
            readonly_common_accdb,
            zip_name, expected,
    ):
        """return_non_empty_df_when_sql_returns_records"""
        zip_path = readonly_common_accdb[zip_name]
        zip_member = zip_path.stem + ".accdb"

        sql = """
            SELECT
            t1.LastName,
            t1.FirstName,
            t1.DateOfBirth,
            t2.LastName as might_be_family_LastName,
            t2.FirstName as might_be_family_FirstName,
            t2.DateOfBirth as might_be_family_DateOfBirth
            FROM MyTable as t1
            LEFT JOIN MyTable as t2
            ON t1.LastName = t2.LastName
            AND (
                t1.FirstName <> t2.FirstName
                OR t1.DateOfBirth <> t2.DateOfBirth
            )
        ;"""

        w_df = msofficepy.access.zip_to_dataframe(
            path=str(zip_path), zip_member=zip_member, sql=sql
        )

        assert len(w_df) == expected
