"""Test msofficepy.excel.grep"""
import pytest
import pathlib

import msofficepy

SCRIPTDIR = pathlib.Path(__file__).parent.resolve()
INPUTDIR = SCRIPTDIR.joinpath("input-file")


class TestGrep:
    """Test excel.grep"""

    def test_raise_given_invalid_path(self, tmp_path):
        """Should raise when invalid_path is given"""
        # given
        path = tmp_path.joinpath("invalid.xlsx").resolve()
        grep_words = ["abc", "def"]

        # when # then
        with pytest.raises(FileNotFoundError):
            assert msofficepy.excel.grep(
                path=path,
                grep_words=grep_words,
            )

    @pytest.mark.parametrize("testcase, grep_words, expect, expect_jp", [
        ("1 word", ["abc"], 8, 4),
        ("1 word with vertical bar", ["abc| def"], 5, 0),
        ("2 words", ["abc", "def"], 9, 4),
        ("1 japanese word", ["あいう"], 0, 8),
        ("1 japanese word with vertical bar", ["あいう| えお"], 0, 5),
        ("2 japanese words", ["あいう", "えお"], 0, 9),
    ])
    def test_grep(self, testcase, grep_words, expect, expect_jp):
        """Test patterns of grep_words

        Input file("grep_testdata.xlsx") has 2 sheets,
        "testdata" and "testdata_jp".
        Parameter "expect" is number of words to match on sheet "testdata".
        Parameter "expect_jp" is the same for "testdata_jp".

        On each sheet, column A has words to be searched.
        Column B through G has a pattern of grep_words on the first row,
        and notes if the words on column A should match it or not.
        """
        # given
        path = str(INPUTDIR.joinpath("grep_testdata.xlsx").resolve())

        # when
        grepped_cells = msofficepy.excel.grep(
            path=path,
            grep_words=grep_words,
        )

        # then
        assert len([
            x for x in grepped_cells
            if x.column == 0 and x.sheet == "testdata"
        ]) == expect
        assert len([
            x for x in grepped_cells
            if x.column == 0 and x.sheet == "testdata_jp"
        ]) == expect_jp
        assert expect + expect_jp < len(grepped_cells)
