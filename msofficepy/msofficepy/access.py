"""Functions to operate access easily."""
import pathlib
import tempfile
import zipfile
import pandas as pd
import pyodbc


def file_to_dataframe(
        path: str,
        sql: str,
) -> pd.DataFrame:
    """Reads access file and returns pandas dataframe."""
    accdb_path = pathlib.Path(path).resolve()
    cnxn_str = (
        r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};'
        r'DBQ=' + str(accdb_path) + ';'
    )
    cnxn = pyodbc.connect(cnxn_str, readonly=True)
    w_df = pd.read_sql(sql, cnxn)
    cnxn.close()
    return w_df


def zip_to_dataframe(
        path: str,
        zip_member: str,
        sql: str,
) -> pd.DataFrame:
    """Reads access file inside a zip file and returns pandas dataframe.

    It tries to read garbled filenames of zip files created by windows.
    [Python で zip 展開（日本語ファイル名対応） - Qiita]
    (https://qiita.com/tohka383/items/b72970b295cbc4baf5ab)
    """
    zip_path = pathlib.Path(path).resolve()

    with tempfile.TemporaryDirectory() as path_str:
        tmp_path = pathlib.Path(path_str).resolve()
        temp_accdb_path = tmp_path.joinpath(zip_member)

        with zipfile.ZipFile(str(zip_path)) as zf:
            if zip_member in zf.namelist():
                zf.extract(zip_member, str(tmp_path))
            else:
                name = zip_member.encode("cp932").decode("cp437")
                if name in zf.namelist():
                    zf.extract(name, str(tmp_path))
                    tmp_path.joinpath(name).rename(str(temp_accdb_path))
                else:
                    zf.extract(zip_member, str(tmp_path))

        w_df = file_to_dataframe(str(temp_accdb_path), sql)
    return w_df
