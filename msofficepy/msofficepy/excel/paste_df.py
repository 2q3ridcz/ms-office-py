"""Functions to operate excel easily."""
import typing
import pathlib
import pandas as pd
import xlwings as xw


def paste_df(
        df: pd.DataFrame,
        target_path: str,
        target_sheet: str,
        target_position: typing.Optional[str] = "A1",
        overwrite_book: bool = False,
        out_path: str = "",
        index: bool = True,
        header: bool = True,
) -> None:
    """Writes dataframe values to excel file."""
    if overwrite_book:
        if out_path != "":
            raise ValueError(
                "out_path should be blank when overwrite_book is True."
            )
    else:
        if out_path == "":
            raise ValueError(
                    "out_path should be set when overwrite_book is False."
            )
    book_path = pathlib.Path(out_path).resolve()

    app = xw.App(visible=False)
    try:
        template_book_path = pathlib.Path(target_path).resolve()
        # TODO: Use new parameters(read_only, password, ...) of books.open.
        work_book = app.books.open(
            fullname=str(template_book_path),
            # read_only=True,
        )

        sheet = work_book.sheets[target_sheet]

        sheet.range(target_position).options(
            index=index,
            header=header
        ).value = df

        if overwrite_book:
            work_book.save()
        else:
            book_path = pathlib.Path(out_path).resolve()
            work_book.save(book_path)
        work_book.close()
    finally:
        app.quit()
