"""conftest"""
import os
import zipfile
import pathlib
import pytest
import pyodbc
import msofficepy.msaccessdb as msaccessdb

CONFTESTDIR = os.path.abspath(os.path.dirname(__file__))


def create_sample_1_empty_table_accdb(path: str) -> None:
    """create_sample_1_empty_table_accdb"""
    msaccessdb.create(path)

    cnxn_str = (
        r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};'
        r'DBQ=' + str(path) + ';'
    )
    cnxn = pyodbc.connect(cnxn_str)
    cursor = cnxn.cursor()

    sql = """CREATE TABLE MyTable (
        LastName CHAR,
        FirstName CHAR,
        DateOfBirth DATETIME
    );"""

    cursor.execute(sql)
    cursor.commit()
    cursor.close()
    cnxn.close()


def create_sample_1_table_accdb(path: str) -> None:
    """create_sample_1_empty_table_accdb"""
    create_sample_1_empty_table_accdb(path=path)

    cnxn_str = (
        r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};'
        r'DBQ=' + str(path) + ';'
    )
    cnxn = pyodbc.connect(cnxn_str)
    cursor = cnxn.cursor()

    # sql = """INSERT INTO MyTable (LastName, FirstName, DateOfBirth) VALUES
    #     ('山中', '一郎', #2020/7/19#),
    #     ('田中', '一郎', #1999/1/1#),
    #     ('山中', '三太郎', #2199/12/31#);
    # """
    # cursor.execute(sql)
    # 上の sqlでは複数行挿入できず。(漢字が原因かと思いアルファベットにしたけどNG。)
    # pyodbc.ProgrammingError: ('42000', '[42000] [Microsoft][ODBC Microsoft
    # Access Driver] Missing semicolon (;) at end of SQL statement. (-3516)
    # (SQLExecDirectW)')

    for last_name, first_name, date_of_birth in [
            ('山中', '一郎', '#2020/7/19#'),
            ('田中', '一郎', '#1999/1/1#'),
            ('山中', '一郎', '#2199/12/31#'),
            ('山中', '次郎', '#2020/7/19#'),
    ]:
        sql = """INSERT INTO MyTable (LastName, FirstName, DateOfBirth)
            VALUES (
                '""" + last_name + """',
                '""" + first_name + """',
                """ + date_of_birth + """
            );
        """
        cursor.execute(sql)
    cursor.commit()
    cursor.close()
    cnxn.close()


@pytest.fixture(scope='function', autouse=True)
def readonly_common_accdb(tmp_path):
    """readonly_common_accdb"""
    file_path_dict = {}

    # create one_empty_table_accdb
    accdb_stem = "one_empty_table_accdb"
    accdb_path = tmp_path.joinpath(accdb_stem + ".accdb").resolve()
    create_sample_1_empty_table_accdb(path=str(accdb_path))
    file_path_dict[accdb_path.name] = accdb_path

    # create and archive one_table_accdb
    accdb_stem = "one_table_accdb"
    accdb_path = tmp_path.joinpath(accdb_stem + ".accdb").resolve()
    create_sample_1_table_accdb(path=str(accdb_path))
    file_path_dict[accdb_path.name] = accdb_path

    zip_path = tmp_path.joinpath(accdb_stem + ".zip").resolve()
    with zipfile.ZipFile(
            zip_path, 'w',
            compression=zipfile.ZIP_DEFLATED,
            compresslevel=9
    ) as zip_file:
        zip_file.write(accdb_path, arcname=accdb_path.name)
    file_path_dict[zip_path.name] = zip_path

    # add cp932 encoded garbled file name zip file
    child_path = "input-file/one_table_日本語ファイル名_accdb.zip"
    zip_path = pathlib.Path(CONFTESTDIR).joinpath(child_path).resolve()
    file_path_dict[zip_path.name] = zip_path

    yield file_path_dict
