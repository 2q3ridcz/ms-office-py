import re
import typing
import pandas as pd

from dataclasses import dataclass


@dataclass(init=False, eq=True, frozen=True)
class GreppedCell:
    column: typing.Any
    index: typing.Any
    cell_value: str
    grep_regex: str
    hit_words: typing.List[str]

    def __init__(
            self,
            column,
            index,
            cell_value,
            grep_regex,
            hit_words,
    ):
        object.__setattr__(self, "column", column)
        object.__setattr__(self, "index", index)
        object.__setattr__(self, "cell_value", cell_value)
        object.__setattr__(self, "grep_regex", grep_regex)
        object.__setattr__(self, "hit_words", hit_words)


def grep_df_by_regex(
    df: pd.DataFrame,
    regex: str,
) -> typing.List[GreppedCell]:
    pattern = re.compile(regex)

    grepped_cells = []
    for col in df.columns:
        found_sr = df[col].astype(str).map(lambda x: pattern.findall(x))
        index = found_sr[found_sr.map(lambda x: x != [])].index

        grepped_cells += [
            GreppedCell(
                column=col,
                index=i,
                cell_value=df[col][i],
                grep_regex=pattern.pattern,
                hit_words=found_sr[i],
            )
            for i in index
        ]
    return grepped_cells
