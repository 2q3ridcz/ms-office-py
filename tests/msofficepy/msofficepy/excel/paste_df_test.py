"""Test msofficepy.excel.paste_df"""
import pytest

import io
import pathlib
import numpy as np
import pandas as pd
from pywintypes import com_error

import msofficepy

SCRIPTDIR = pathlib.Path(__file__).parent.resolve()
INPUTDIR = SCRIPTDIR.joinpath("input-file")


# Set ENABLE_TEST to True to test.
# Aware testcases here needs:
# - excel installed to use xlwings
ENABLE_TEST = False
SKIP_IF = pytest.mark.skipif(
    not ENABLE_TEST,
    reason='unsure if excel is installed',
)


@SKIP_IF
class TestPasteDf:
    """Test excel.paste_df"""

    def test_raise_given_invalid_path(self, tmp_path):
        """Should raise when invalid path is given"""
        # given
        file = tmp_path.joinpath("invalid.xlsx").resolve()
        assert not file.exists()
        path = str(file)
        sheet_name = "invalid"
        target_position = "B3"
        out_path = str(tmp_path.joinpath("test_raise_given_invalid_path.xlsx").resolve())

        df = pd.DataFrame(data={
            'name': ['太郎', '次郎', '三郎', ],
            'score': [100, 200, 300, ],
        })

        # when # then
        with pytest.raises(FileNotFoundError):
            assert msofficepy.excel.paste_df(
                df=df,
                target_path=path,
                target_sheet=sheet_name,
                target_position=target_position,
                out_path=out_path,
            )

    def test_raise_given_invalid_sheet_name(self, tmp_path):
        """Should raise when invalid sheet_name is given"""
        # given
        file = INPUTDIR.joinpath("paste_df_testdata.xlsx").resolve()
        assert file.exists()
        path = str(file)
        sheet_name = "invalid"
        target_position = "B3"
        out_path = str(tmp_path.joinpath("test_raise_given_invalid_sheet_name.xlsx").resolve())

        df = pd.DataFrame(data={
            'name': ['太郎', '次郎', '三郎', ],
            'score': [100, 200, 300, ],
        })

        # when # then
        with pytest.raises(com_error):
            assert msofficepy.excel.paste_df(
                df=df,
                target_path=path,
                target_sheet=sheet_name,
                target_position=target_position,
                out_path=out_path,
            )

    @pytest.mark.parametrize("testcase, index, header, expect", [
        ("false-false", False, False, [
            ['成績表', '2年 A組', None],
            [None, None, None],
            [None, '氏名', '3科目合計'],
            [None, '太郎', 100],
            [None, '次郎', 200],
            [None, '三郎', 300],
        ]),
        ("false-true", False, True, [
            ['成績表', '2年 A組', None],
            [None, None, None],
            [None, '氏名', '3科目合計'],
            [None, 'name', 'score'],
            [None, '太郎', 100],
            [None, '次郎', 200],
            [None, '三郎', 300],
        ]),
        ("true-false", True, False, [
            ['成績表', '2年 A組', None, None],
            [None, None, None, None],
            [None, '氏名', '3科目合計', None],
            [None, 0, '太郎', 100],
            [None, 1, '次郎', 200],
            [None, 2, '三郎', 300],
        ]),
        ("true-true", True, True, [
            ['成績表', '2年 A組', None, None],
            [None, None, None, None],
            [None, '氏名', '3科目合計', None],
            [None, None, 'name', 'score'],
            [None, 0, '太郎', 100],
            [None, 1, '次郎', 200],
            [None, 2, '三郎', 300],
        ]),
    ])
    def test_file_to_df(self, tmp_path, testcase, index, header, expect):
        """Reads excel file and returns pandas dataframe"""
        # given
        file = INPUTDIR.joinpath("paste_df_testdata.xlsx").resolve()
        assert file.exists()
        path = str(file)
        sheet_name = "成績表"
        target_position = "B4"
        out_path = str(tmp_path.joinpath("test_file_to_df-" + testcase + ".xlsx").resolve())

        df = pd.DataFrame(data={
            'name': ['太郎', '次郎', '三郎', ],
            'score': [100, 200, 300, ],
        })

        # when
        msofficepy.excel.paste_df(
            df=df,
            target_path=path,
            target_sheet=sheet_name,
            target_position=target_position,
            out_path=out_path,
            index=index,
            header=header,
        )

        # then
        acctual = pd.read_excel(out_path, sheet_name=sheet_name, header=None)
        expect_df = pd.DataFrame(data=expect)
        pd.testing.assert_frame_equal(acctual, expect_df)
