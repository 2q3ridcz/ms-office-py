"""Test msofficepy.excel.file_to_df"""
import pytest
import io
import pathlib
import numpy as np
import pandas as pd
from pywintypes import com_error

import msofficepy

SCRIPTDIR = pathlib.Path(__file__).parent.resolve()
INPUTDIR = SCRIPTDIR.joinpath("input-file")


# Set ENABLE_TEST to True to test.
# Aware testcases here needs:
# - excel installed to use xlwings
# - human operation (have to enter password)
ENABLE_TEST = False
SKIP_IF = pytest.mark.skipif(
    not ENABLE_TEST,
    reason='not automated, and unsure if excel is installed',
)


@SKIP_IF
class TestFileToDf:
    """Test excel.file_to_df"""

    def test_raise_given_invalid_path(self, tmp_path):
        """Should raise when invalid path is given"""
        # given
        path = tmp_path.joinpath("invalid.xlsx").resolve()
        sheet_name = "invalid"

        # when # then
        with pytest.raises(FileNotFoundError):
            assert msofficepy.excel.file_to_df(
                path=path,
                sheet_name=sheet_name,
            )

    def test_raise_given_invalid_sheet_name(self):
        """Should raise when invalid sheet_name is given"""
        # given
        path = str(INPUTDIR.joinpath("file_to_df_testdata.xlsx").resolve())
        sheet_name = "invalid"

        # when # then
        with pytest.raises(com_error):
            assert msofficepy.excel.file_to_df(
                path=path,
                sheet_name=sheet_name,
            )

    def test_file_to_df(self):
        """Reads excel file and returns pandas dataframe"""
        # given
        path = str(INPUTDIR.joinpath("file_to_df_testdata.xlsx").resolve())
        sheet_name = "target"

        # when
        df = msofficepy.excel.file_to_df(
            path=path,
            sheet_name=sheet_name,
            # password="passw0rd"
        )

        # then
        expect = pd.DataFrame(data=[
            [None, None, 'b', None],
            ['a', None, None, None],
            [None, 'c', None, None],
            [None, None, None, None],
            [None, None, None, 'e'],
            [None, None, None, None],
            [None, 'g', None, None],
        ])
        pd.testing.assert_frame_equal(df, expect)
