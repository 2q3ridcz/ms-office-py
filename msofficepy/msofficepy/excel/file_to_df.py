"""Functions to operate excel easily."""
import pathlib
import pandas as pd
import xlwings as xw


def file_to_df(
        path: str,
        sheet_name: str,
        # password: typing.Optional[str] = None,
) -> pd.DataFrame:
    """Reads excel file and returns pandas dataframe.

    It can read from password protected excel files, which pandas can't open.
    For unprotected excel files, pandas is recommended.

    Do not use this function for automation.
    Excel dialog will show up and demand password when opening password
    protected excel files.
    """
    book_path = pathlib.Path(path).resolve()

    app = xw.App(visible=False)
    try:
        # TODO: Use new parameters(read_only, password, ...) of books.open.
        work_book = app.books.open(
            fullname=str(book_path),
            # read_only=True,
            # password=password,
        )

        sheet_value = work_book.sheets[sheet_name].used_range.value
        sheet_df = pd.DataFrame(sheet_value)
    finally:
        app.quit()
    return sheet_df
