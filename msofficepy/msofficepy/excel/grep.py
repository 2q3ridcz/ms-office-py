"""Functions to operate excel easily."""
import typing
import pathlib
import re
import pandas as pd

from dataclasses import dataclass

from ..df_util.grep_df_by_regex import grep_df_by_regex


@dataclass(init=False, eq=True, frozen=True)
class MatchCell:
    path: str
    file: str
    sheet: str
    row: int
    column: int
    value: str
    grep_words: typing.List[str]
    hit_words: typing.List[str]

    def __init__(
            self,
            path,
            file,
            sheet,
            row,
            column,
            value,
            grep_words,
            hit_words,
    ):
        object.__setattr__(self, "path", path)
        object.__setattr__(self, "file", file)
        object.__setattr__(self, "sheet", sheet)
        object.__setattr__(self, "row", row)
        object.__setattr__(self, "column", column)
        object.__setattr__(self, "value", value)
        object.__setattr__(self, "grep_words", grep_words)
        object.__setattr__(self, "hit_words", hit_words)


def grep(
    path: str,
    grep_words: typing.List[str],
) -> typing.List[MatchCell]:
    book_path = pathlib.Path(path).resolve()
    df_dict = pd.read_excel(
            book_path,
            sheet_name=None,
            header=None,
            dtype=str
    )
    grepped_cell_list = []
    for sheet_name, df in df_dict.items():
        regex = "|".join([re.escape(x) for x in grep_words])
        result_list = grep_df_by_regex(df=df, regex=regex)
        grepped_cell_list += [MatchCell(
                path=str(book_path),
                file=book_path.name,
                sheet=sheet_name,
                row=int(x.index),
                column=int(x.column),
                value=x.cell_value,
                grep_words=grep_words,
                hit_words=x.hit_words,
        ) for x in result_list]
    return grepped_cell_list
