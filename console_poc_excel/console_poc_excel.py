"""main"""
import os
import pathlib
import sys
import pandas as pd

PKGPATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if PKGPATH not in sys.path:
    sys.path.insert(0, PKGPATH)

import msofficepy


def main() -> None:
    """main"""
    input_folder_path = pathlib.Path("./Input")
    output_folder_path = pathlib.Path("./Output")
    path = output_folder_path.joinpath("template_updated.xlsx").resolve()
    template_path = input_folder_path.joinpath("template.xlsx").resolve()
    sheet_name = "sample_sheet"

    df = pd.DataFrame(data={
        'name': ['太郎', '次郎', '三郎', ],
        'score': [100, 200, 300, ],
    })

    print("path.exists() :", path.exists())

    msofficepy.excel.paste_df(
        df=df,
        target_path=str(template_path),
        target_sheet=sheet_name,
        target_position="B3",
        out_path=str(path),
    )

    print("path.exists() :", path.exists())


if __name__ == '__main__':
    main()
