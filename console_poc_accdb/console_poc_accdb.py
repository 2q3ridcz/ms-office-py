"""main"""
import os
import pathlib
import sys
import pandas as pd

PKGPATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if PKGPATH not in sys.path:
    sys.path.insert(0, PKGPATH)

import msofficepy


def main() -> None:
    """main"""
    intput_folder_path = pathlib.Path("./Input").resolve()

    zip_path = intput_folder_path.joinpath("poc.zip")
    zip_member = "poc.accdb"

    sql = """
        SELECT
        t1.LastName,
        t1.FirstName,
        t1.DateOfBirth,
        t2.LastName as might_be_family_LastName,
        t2.FirstName as might_be_family_FirstName,
        t2.DateOfBirth as might_be_family_DateOfBirth
        FROM MyTable as t1
        LEFT JOIN MyTable as t2
        ON t1.LastName = t2.LastName
        AND (
            t1.FirstName <> t2.FirstName
            OR t1.DateOfBirth <> t2.DateOfBirth
        )
    ;"""

    w_df = msofficepy.access.zip_to_dataframe(
        path=str(zip_path), zip_member=zip_member, sql=sql
    )

    print("")
    print('Len :', len(w_df))

    print("")
    print('Info :')
    w_df.info()

    print("")
    print('Trimming string values for presentation...')
    for w_col in w_df.columns:
        if str(w_col).endswith("Name"):
            w_df[w_col] = w_df[w_col].str.strip()

    print("")
    print('Display :')
    with pd.option_context(
            'display.max_rows', None, 'display.max_columns', None
    ):
        print(w_df)


if __name__ == '__main__':
    main()
