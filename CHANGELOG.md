# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2023-01-27
### Added
- access.file_to_dataframe
- access.zip_to_dataframe
- excel.file_to_df
- excel.paste_df
- excel.grep

[Unreleased]: https://gitlab.com/2q3ridcz/ms-office-py/-/compare/v0.1.0...main
[0.1.0]: https://gitlab.com/2q3ridcz/ms-office-py/-/tree/v0.1.0
